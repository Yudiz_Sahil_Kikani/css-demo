import React from 'react'
import './App.css'
import img from './Assets/img/logo.png'
import sr1 from './Assets/img/bg.jpg'
import sr2 from './Assets/img/bg2.jpg'
import sr3 from './Assets/img/bg3.jpg'
import tm from './Assets/img/profile.jpg'
import bd from './Assets/img/bd.jpg'
import bd1 from './Assets/img/bd1.jpg'
import bd2 from './Assets/img/bd2.jpg'
import bd3 from './Assets/img/bd3.jpg'
import { BiCast } from 'react-icons/bi'
import { FiLayers } from 'react-icons/fi'
import { BsPeople } from 'react-icons/bs'
import { AiFillFacebook, AiFillLinkedin, AiFillTwitterCircle } from 'react-icons/ai'

function App () {
  return (
    <div className='container'>
         <div className='app-navbar'>
      <div className='app-logo'>
        <img className='logo' src={img}/>
      </div>
      <div className='nav-links'>
        <a>Home</a>
        <a>Services</a>
        <a>About</a>
        <a>Pages</a>
        <a>Blocks</a>
        <a>Contact</a>
        <button className='btn-1'>Buy now</button>
      </div>
     </div>
     <div className='profile-page'>
      <div className='page-content'>
        <h1>AGENCY.</h1>
        <p>There are many varients of passages but the majority</p>
        <p>have sufferend ultration</p>
        <button className='btn-2'>CONTACT US</button>
        </div>
     </div>
  <div className='var'>
     <div className='app-var'>
      <div className='symbole'><BiCast/></div>
        <h3>Business Strategy</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>

     <div className='app-var'>
      <div className='symbole'><FiLayers/></div>
        <h3>Website Development</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>

     <div className='app-var'>
      <div className='symbole'><BsPeople/></div>
        <h3>Marketing & Services</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>
     </div>

     <div className='services'>

      <div className='service-title'>
        <h1>Services</h1>
        <p>Theare are many varients of loreipusm is availble but this is best</p>
        <div className='read-more'><span className='read'>More</span><span> Service</span></div>
      </div>

      <div className='service-name'>
        <div><img className='sr-img' src={sr1}/></div>
        <h3>Thinking Development</h3>
        <p>I throw My self down the among the tall grass By the stream</p>
        <div className='read-more'><span className='read'>Read</span><span> More</span></div>
        </div>

        <div className='service-name'>
        <div><img className='sr-img' src={sr2}/></div>
        <h3>Thinking Development</h3>
        <p>I throw My self down the among the tall grass By the stream</p>
        <div className='read-more'><span className='read'>Read</span><span> More</span></div>
        </div>

        <div className='service-name'>
        <div><img className='sr-img' src={sr3}/></div>
        <h3>Thinking Development</h3>
        <p>I throw My self down the among the tall grass By the stream</p>
        <div className='read-more'><span className='read'>Read</span><span> More</span></div>
        </div>

     </div>

     <div className='staff'>
      <div className='staff-count'>
        <h1>992+</h1>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth have sufferend ultration</p>
      </div>

      <div className='staff-count'>
        <h1>575+</h1>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth have sufferend ultration</p>
      </div>

      <div className='staff-count'>
        <h1>79+</h1>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth have sufferend ultration</p>
      </div>

     </div>

     <div className='working'>
      <div>
        <img className='bd' src={tm}/>
      </div>
      <div className='process'>
        <h1 className='process-name'>Working Process</h1>
        <p>
        A work process is any method that employees use to create value for the company. Companies often create company or industry-specific work processes to accomplish tasks. For example, a manufacturing company might create a specific work process for producing its clothing line, while a marketing company might create a work process for establishing a new social media ad campaign. You can include almost anything employees do to create value and complete projects in the definition of a work process.
        </p>
      </div>

     </div>

     <div className='team'>
    <h1>Managing Team</h1>
    <p>A work process is any method that employees use to create value for the company. Companies often create company or industry-specific work processes to accomplish tasks</p>
     </div>

     <div className='managing'>
      <div className='developer'>
        <img className='dev-pic' src={bd}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><AiFillFacebook/></span><span><AiFillLinkedin/></span><span><AiFillTwitterCircle/></span></div>
      </div>

      <div className='developer'>
        <img className='dev-pic' src={bd1}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><AiFillFacebook/></span><span><AiFillLinkedin/></span><span><AiFillTwitterCircle/></span></div>
      </div>

      <div className='developer'>
        <img className='dev-pic' src={bd2}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><AiFillFacebook/></span><span><AiFillLinkedin/></span><span><AiFillTwitterCircle/></span></div>
      </div>

      <div className='developer'>
        <img className='dev-pic' src={bd3}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><AiFillFacebook/></span><span><AiFillLinkedin/></span><span><AiFillTwitterCircle/></span></div>
      </div>
     </div>

</div>
  )
}

export default App
